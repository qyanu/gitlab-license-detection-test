*******************************
 GitLab License Detection Test
*******************************

gitlab did detect the license of an uploaded repository "wrong" (as in:
sufficiently different from the intended license), so i created
this repository here to test certain variants of declaring a license
and what gitlab detects.

An important note is, that i was unable to find any documentation by
GitLab on the license-detection mechanism at work for repositories
uploaded to gitlab.com/


Branches
========

just-gpl2-file
    in this branch, the GNU Public License version 2 was copied, as is,
    from the file /usr/share/common-licenses/GPL-2 as distributed by
    debian bookworm 12.0 to the file ``COPYING``.

gpl2-file-with-recommended-application
    in this branch, the recommendations of the GPLv2 on how to apply
    the license are followed to the letter.

gpl2-file-with-prefix-note
    in this branch, the GPLv2 is applied with a prefixing note inside
    the COPYING file. The prefixing note applies the license to the
    whole project (which is nowhere done inside the text of the GPLv2
    itself).
